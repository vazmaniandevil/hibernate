/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package hibernateweek;

import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<Customer> c = t.getCustomers();
        for (Customer i : c) {
            System.out.println(i);
        }

        System.out.println(t.getCustomer(3));

        insertNewCustomer();
        for (Customer i : c) {
            System.out.println(i);
        }
    }

    public static void insertNewCustomer(){
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        String name;
        String address;
        String phone;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Please, Enter the following information:\n");

        System.out.print("\n Name: ");
        name = scanner.nextLine();

        System.out.print("\n Address: ");
        address = scanner.nextLine();

        System.out.print("\n Phone: ");
        phone = scanner.nextLine();

        Customer newCustomer = new Customer();
        newCustomer.setName(name);
        newCustomer.setAddress(address);
        newCustomer.setPhone(phone);

        session.save(newCustomer);
        session.getTransaction().commit();
        HibernateUtils.shutdown();
    }
}

